
#ifndef _libBBB_H_
#define _libBBB_H_

//Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>

//Type definitions
struct PWMpins{
    char name[10];
    int period;
    int duty;
    int enable;
};

//Definitions
#define OUT	"out"
#define IN	"in"
#define ON	1
#define OFF	0

#define USR0    "usr0"
#define USR1	"usr1"
#define USR2	"usr2"
#define USR3	"usr3"
#define E	65
#define RS	27
#define D4	46
#define D5	47
#define D6	26
#define D7	44
#define AIN0	"0"
#define AIN1	"1"
#define AIN2	"2"
#define AIN3	"3"
#define AIN4	"4"
#define AIN5	"5"
#define AIN6	"6"
#define AIN7	"7"

//Device Tree Overlay
int addOverlay(char *dtb, char *overname);

//USR Prototypes
//int setUsrLedValue(char* led, int value);

//GPIO Prototypes
int initPin(int pinnum);           
int setPinDirection(int pinnum, char* dir);
int setPinValue(int pinnum, int value);
int getPinValue(int pinnum);

//PWM Prototypes
void configPWMPin(struct PWMpins *pin);
void setPWMPeriod(struct PWMpins *pin);
void setPWMDuty(struct PWMpins *pin);
void setPWMOnOff(struct PWMpins *pin);

//Analog
int readAnalog(char *pin);

//Time Prototypes
void pauseSec(int sec);
int  pauseNanoSec(long nano);

#endif
