
#include "libBBB.h"

//Local functions not used by outside world
int checkOverlay(char *file);
void initCMD(unsigned char cmd);

//*************************************************
//*            Device Tree Overlay                *
//*************************************************
int addOverlay(char *dtb, char *overname)
{
	char file[100];

	sprintf(file,"/lib/firmware/%s",dtb);

	printf("Check for dtbo file: ");
	if(checkOverlay(file) == 0)
	{
		//check current directory for file
		char temp[100];
		sprintf(temp,"%s",dtb);

		if((access(temp,F_OK)) < 0)
		{
			printf("The file isn't here either\n");
			exit(1);
		}
		else
		{
			printf("Found file\n");
			printf("Copying to correct folder\n");

			FILE *f,*fnew;
			long num;
			char *fileread;
			size_t result;

			f = fopen(temp,"r");
			if(f == NULL)
			{
				printf("temp failed to open\n");
				exit(1);
			}

			fnew = fopen(file,"w");
			if(fnew == NULL)
			{
				printf("file failed to open\n");
				exit(1);
			}

			fseek(f,0,SEEK_END);
			fseek(fnew,0,SEEK_SET);
			num = ftell(f);
			rewind(f);

			fileread = (char*) malloc(sizeof(char)*num);
			if(fileread == NULL)
			{
				printf("Memory error\n");
				exit(1);
			}

			result = fread(fileread,1,num,f);
			if(result != num)
			{
				printf("Error reading file\n");
				exit(1);
			}

			fwrite(fileread,sizeof(char),sizeof(fileread),fnew);
			fclose(f);
			fclose(fnew);

			//Now file should be there so double check
			if(checkOverlay(file) == 0)
			{
				printf("This just isn't working\n");
				exit(1);
			}
		}

	}

	//If you made it here then the dtbo file exists now we need to check
	//if it is already applied or not, then add it if it isn't there, or
	//if it is already applied then we did all of this for nothing
	
	FILE *over;
	char ch[200] = {0};
	const char *pch = &ch[0];
	char *search;
	int ch2 = 0;
	int j;
	//char strsearch[] = "uart";

	over = fopen("/sys/devices/platform/bone_capemgr/slots", "r");
	if(over == NULL)
	{
		printf("File didn't open\n");
		exit(1);
	}

	fseek(over,0,SEEK_SET);

	while(ch2 != -1)
	{
		//find all the overlays
		j = 0;
		while(ch2 != '\n')
		{
			ch2 = fgetc(over);
			if(ch2 == '\n')
				break;
			ch[j] = (char) ch2;
			j++;
		}

		printf("%s\n",ch);

		//now look for the specific overlay
		search = strstr(pch,overname);
		if(search == NULL)
		{
			printf("Search: Failed\n");
		}
		else
		{
			printf("Search: Found\n");
			return 0;
		}

		ch2 = fgetc(over);

	}

	fclose(over);

	//ok if you made it here then you search them all and it would
	//appear that it isn't there, so it is now time to add it
	char name[100];
	sprintf(name, "%s",overname);

	over = fopen("/sys/devices/platform/bone_capemgr/slots", "w");
	if(over == NULL) printf("File didn't open\n");
	fseek(over,0,SEEK_SET);
	fprintf(over,name);
	fflush(over);
	fclose(over);

	printf("Overlay Added\n");	
	return 0;
}

int checkOverlay(char *file)
{
	int found = 0;

	if((access(file,F_OK)) < 0)
	{
		printf("Failed\n");
		found = 0;
	}
	else
	{
		printf("Success\n");
		found = 1;
	}
	return found;
}

//*************************************************
//*                USR FUNCTIONS                  *
//*************************************************
/*
void setUsrLedValue(char* led, int value){
	FILE *usr;
	char buf[20];
	char buf2[50] = "/sys/class/leds/beaglebone:green:";

	//build file path to usr led brightness
	sprintf(buf,"%s",led);
	strcat(buf2,strcat(buf,"/brightness"));

	usr = fopen(buf2, "w");
	if(usr == NULL) printf("USR Led failed to open\n");
	fseek(usr,0,SEEK_SET);
	fprintf(usr,"%d",value);
	fflush(usr);
	fclose(usr);

}

void setTrigger(char* led){
	
}

*/
//*************************************************
//*               GPIO FUNCTIONS                  *
//*************************************************
/*
void initPin(int pinnum)
{
	FILE *io;

	io = fopen("/sys/class/gpio/export", "w");
	if(io == NULL) printf("Pin failed to initialize\n");
	fseek(io,0,SEEK_SET);
	fprintf(io,"%d",pinnum);
	fflush(io);
	fclose(io);

}

void setPinDirection(int pinnum, char* dir)
{
	FILE *pdir;
	char buf[10];
	char buf2[50] = "/sys/class/gpio/gpio";

	//build file path to the direction file
	sprintf(buf,"%i",pinnum);
	strcat(buf2,strcat(buf,"/direction"));

	pdir = fopen(buf2, "w");
	if(pdir == NULL) printf("Direction failed to open\n");
	fseek(pdir,0,SEEK_SET);
	fprintf(pdir,"%s",dir);
	fflush(pdir);
	fclose(pdir);

}

void setPinValue(int pinnum, int value)
{
	FILE *val;
	char buf[5];
	char buf2[50] = "/sys/class/gpio/gpio";

	//build path to value file
	sprintf(buf,"%i",pinnum);
	strcat(buf2,strcat(buf,"/value"));

	val = fopen(buf2, "w");
	if(val == NULL) printf("Value failed to open\n");
	fseek(val,0,SEEK_SET);
	fprintf(val,"%d",value);
	fflush(val);
	fclose(val);

}

int getPinValue(int pinnum)
{
	FILE *val;
	int value;
	char buf[5];
	char buf2[50] = "/sys/class/gpio/gpio";

	//build file path to value file
	sprintf(buf,"%i",pinnum);
	strcat(buf2,strcat(buf,"/value"));

	val = fopen(buf2, "r");
	if(val == NULL) printf("Input value failed to open\n");
	fseek(val,0,SEEK_SET);
	fscanf(val,"%d",&value);
	fclose(val);

	return value;
}
*/
//*************************************************
//*                PWM FUNCTIONS                  *
//*************************************************
void configPWMPin(struct PWMpins *pin){
	FILE *file;
	char dest[75] = "";
	char destExport[100] = "";
	char buf1[50] = "/sys/devices/platform/ocp/ocp:";
	char buf2[20] = "_pinmux/state";
	
	//change pin to pwm
	//build file paths
	strcat(dest, buf1);
	strcat(dest, pin->name);
	strcat(dest, buf2);
	//print file paths
	//printf("pwm file location : \n %s \n", dest);
	
	//change pin to pwm
	file = fopen(dest, "w");
	if(file == NULL){
		perror("Error");
		printf("\n pwm file failed to open \n");
	}
	fprintf(file, "%s", "pwm");
	fclose(file);
	
	//export pin 
	//strcat(destExport, "/sys/devices/platform/ocp/48302000.epwmss/48302200.pwm/pwm/pwmchip3/export");
	
	if(strcmp(pin->name, "P8_13") == 0 || strcmp(pin->name, "P8_19") == 0){
		strcat(destExport, "/sys/class/pwm/pwmchip6/export");
		//printf("\n %s \n", destExport);
		//printf("pwm export file location : \n %s \n", destExport);
		file = fopen(destExport, "w");
		if(file == NULL){
			perror("Error");
			printf("\n pwm export file failed to open \n");
		}else{
			if(strcmp(pin->name, "P8_19") == 0){
				fprintf(file, "%s", "0");//A
			}else{
				fprintf(file, "%s", "1");//B
			}
		}
	}else if(strcmp(pin->name, "P9_21") == 0 || strcmp(pin->name, "P9_22") == 0){
		strcat(destExport, "/sys/class/pwm/pwmchip1/export");
		file = fopen(destExport, "w");
		if(file == NULL){
			perror("Error");
			printf("\n pwm export file failed to open \n");
		}else{
			if(strcmp(pin->name, "P9_22") == 0){
				fprintf(file, "%s", "0");//A
			}else{
				fprintf(file, "%s", "1");//B
			}
		}
	}else if(strcmp(pin->name, "P9_14") == 0 || strcmp(pin->name, "P9_16") == 0){
		strcat(destExport, "/sys/class/pwm/pwmchip3/export");
		file = fopen(destExport, "w");
		if(file == NULL){
			perror("Error");
			printf("\n pwm export file failed to open \n");
		}else{
			if(strcmp(pin->name, "P9_14") == 0){
				fprintf(file, "%s", "0");//A
			}else{
				fprintf(file, "%s", "1");//B
			}
		}
	}
	fclose(file);
}

void setPWMPeriod(struct PWMpins *pin){
	FILE *file;
	char dest[50] = "/sys/class/pwm/pwm-";

	//build file path
		if(strcmp(pin->name, "P8_13") == 0 || strcmp(pin->name, "P8_19") == 0){
		if(strcmp(pin->name, "P8_19") == 0){
			strcat(dest, "6:0");

		}else{
			strcat(dest, "6:1");
		}
	}else if(strcmp(pin->name, "P9_14") == 0 || strcmp(pin->name, "P9_16") == 0){
		if(strcmp(pin->name, "P9_14") == 0){
			strcat(dest, "3:0");

		}else{
			strcat(dest, "3:1");
		}
	}else if(strcmp(pin->name, "P9_21") == 0 || strcmp(pin->name, "P9_22") == 0){
		if(strcmp(pin->name, "P9_22") == 0){
			strcat(dest, "1:0");

		}else{
			strcat(dest, "1:1");
		}
	}
	strcat(dest, "/period");
	//printf("pwm period file location : \n %s \n", dest);
	file = fopen(dest, "w");
	if(file == NULL){
		perror("Error");
		printf("\n pwm period file failed to open \n");
	}
	fprintf(file, "%d", pin->period);
	fclose(file);

}

void setPWMDuty(struct PWMpins *pin){
	
	//printf("\n In duty : %i \n", pin->duty);
	
	FILE *file;
	char dest[50] = "/sys/class/pwm/pwm-";

	//build file path
	if(strcmp(pin->name, "P8_13") == 0 || strcmp(pin->name, "P8_19") == 0){
		if(strcmp(pin->name, "P8_19") == 0){
			strcat(dest, "6:0");

		}else{
			strcat(dest, "6:1");
		}
	}else if(strcmp(pin->name, "P9_14") == 0 || strcmp(pin->name, "P9_16") == 0){
		if(strcmp(pin->name, "P9_14") == 0){
			strcat(dest, "3:0");

		}else{
			strcat(dest, "3:1");
		}
	}else if(strcmp(pin->name, "P9_21") == 0 || strcmp(pin->name, "P9_22") == 0){
		if(strcmp(pin->name, "P9_22") == 0){
			strcat(dest, "1:0");

		}else{
			strcat(dest, "1:1");
		}
	}
	strcat(dest, "/duty_cycle");
	//printf("pwm duty cycle file location : \n %s \n", dest);
	file = fopen(dest, "w");
	if(file == NULL){
		perror("Error");
		printf("\n pwm duty cycle file failed to open \n");
	}
	fprintf(file, "%i", pin->duty);
	fclose(file);

}

void setPWMOnOff(struct PWMpins *pin)
{
	FILE *file;
	//int run = 0;
	char dest[50] = "/sys/class/pwm/pwm-";

	//build file path
		if(strcmp(pin->name, "P8_13") == 0 || strcmp(pin->name, "P8_19") == 0){
		if(strcmp(pin->name, "P8_19") == 0){
			strcat(dest, "6:0");

		}else{
			strcat(dest, "6:1");
		}
	}else if(strcmp(pin->name, "P9_14") == 0 || strcmp(pin->name, "P9_16") == 0){
		if(strcmp(pin->name, "P9_14") == 0){
			strcat(dest, "3:0");

		}else{
			strcat(dest, "3:1");
		}
	}else if(strcmp(pin->name, "P9_21") == 0 || strcmp(pin->name, "P9_22") == 0){
		if(strcmp(pin->name, "P9_22") == 0){
			strcat(dest, "1:0");

		}else{
			strcat(dest, "1:1");
		}
	}
	strcat(dest, "/enable");
	//file = fopen(dest, "r");
	file = fopen(dest, "w");
	//printf("pwm enable file location : \n %s \n", dest);
	if(file == NULL){
		perror("Error");
		printf("\n pwm enable file failed to open \n");
	}
	fprintf(file, "%i", pin->enable);
	fclose(file);

}

//********************************************
//*               Analog                     *
//********************************************
int readAnalog(char *pin){
	FILE *file;
	char value[5];
	char dest[50] = "/sys/bus/iio/devices/iio:device0/in_voltage";
	//char last;
	
	//gets last char, which is the # from pin name
	//last = pin[ (strlen(pin)-1) ];
	//printf("%c", last);
	
	
	strcat(dest, pin);
	strcat(dest, "_raw");
	printf("\n %s \n", dest);
	
	file = fopen(dest, "r");
	fscanf(file, "%s", value);
	//printf("\n %s \n", value);
	fclose(file);
	
	return	atoi(value);
}



//********************************************
//*            TIME FUNCTIONS                *
//********************************************
void pauseSec(int sec)
{
	time_t now,later;

	now = time(NULL);
	later = time(NULL);

	while((later - now) < (double)sec)
		later = time(NULL);
}

int pauseNanoSec(long nano)
{
	struct timespec tmr1,tmr2;

	//assume you are not trying to pause more than 1s
	tmr1.tv_sec = 0;
	tmr1.tv_nsec = nano;

	if(nanosleep(&tmr1, &tmr2) < 0)
	{
		printf("Nano second pause failed\n");
		return -1;
	}
	return 0;
}
